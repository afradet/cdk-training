all: build_back build_front build_stack

build_back:
	cd back && mvn clean install -DskipTests

build_front:
	cd front && npm run build --omit=dev

build_stack:
	cd deploy && cdk synth
