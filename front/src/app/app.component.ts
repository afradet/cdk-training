import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {tap} from "rxjs";

export interface VanillaResponse {
  content: string
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, HttpClientModule, FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'app-front';
  username: string = '';
  customGreetings = '';

  constructor(private httpClient: HttpClient) {
  }

  getGreetings()  {
    return this.httpClient
    .post<VanillaResponse>("api/vanilla/greetings", { username: this.username })
    .pipe(
      tap(res => this.customGreetings = res.content)
    ).subscribe();
  }
}
