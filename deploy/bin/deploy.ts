#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import {Certificate} from "../lib/certificate";
import {FrontendStack} from "../lib/frontend-stack";
import {BackendStack} from "../lib/backend-stack";
import {CloudfrontStack} from "../lib/cloudfront-stack";

const app = new cdk.App();

const certificateStack = new Certificate(app, 'certificateStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: 'us-east-1',
  },
  crossRegionReferences: true, // needed for us-east certificate with cloudfront
});

const frontend = new FrontendStack(app, 'FrontendStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
  },
  crossRegionReferences: true
});

const backend = new BackendStack(app, 'BackendStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
  },
  crossRegionReferences: true
});

new CloudfrontStack(app, 'CloudFrontStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
  },
  crossRegionReferences: true,
  certificate: certificateStack.certificate,
  apiGw: backend.api,
  bucket: frontend.bucket,
  bucketAccessIdentity: frontend.bucketAccessIdentity,
  hostedZone: certificateStack.hostedZone
})

cdk.Tags.of(app).add('afr-project', 'cdktraining');
cdk.Tags.of(app).add('afr-manager', 'cdk');
