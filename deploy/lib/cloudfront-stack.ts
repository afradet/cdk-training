import * as cdk from 'aws-cdk-lib';
import {StackProps} from 'aws-cdk-lib';
import * as acm from "aws-cdk-lib/aws-certificatemanager";
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as apigw from "aws-cdk-lib/aws-apigatewayv2";
import {Construct} from "constructs";
import {
  CloudFrontAllowedMethods,
  CloudFrontWebDistribution,
  HttpVersion,
  OriginAccessIdentity,
  PriceClass,
  ViewerCertificate,
  ViewerProtocolPolicy
} from "aws-cdk-lib/aws-cloudfront";
import {CloudFrontTarget} from "aws-cdk-lib/aws-route53-targets";
import {ARecord, IHostedZone} from "aws-cdk-lib/aws-route53";

export interface CloudfrontStackProps extends StackProps {
  certificate: acm.Certificate;
  bucket: s3.Bucket;
  bucketAccessIdentity: OriginAccessIdentity;
  apiGw: apigw.HttpApi;
  hostedZone: IHostedZone;
}

export class CloudfrontStack extends cdk.Stack {

  constructor(scope: Construct, id: string, props: CloudfrontStackProps) {
    super(scope, id, props);

    const cloudFrontDistrib = new CloudFrontWebDistribution(this, 'cloudfront-distribution', {
      viewerCertificate: ViewerCertificate.fromAcmCertificate(props.certificate, {
        aliases: ['cdktraining.afradet.fr']
      }),
      viewerProtocolPolicy: ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
      httpVersion: HttpVersion.HTTP2,
      priceClass: PriceClass.PRICE_CLASS_100,
      loggingConfig: {
        bucket: new s3.Bucket(this, 'cfn-bucket-log', { objectOwnership: s3.ObjectOwnership.OBJECT_WRITER })
      },
      originConfigs: [
        {
          customOriginSource: {
            domainName: `${props.apiGw.httpApiId}.execute-api.${this.region}.amazonaws.com`
          },
          behaviors: [
            {
              pathPattern: '/api',
              compress: true,
              allowedMethods: CloudFrontAllowedMethods.ALL
            }
          ]
        },
        {
          s3OriginSource: {
            s3BucketSource: props.bucket,
            originAccessIdentity: props.bucketAccessIdentity
          },
          behaviors: [{
            compress: true,
            isDefaultBehavior: true
          }]
        }
      ],
      // // Allow Angular SPA to handle all errors internally
      // errorConfigurations: [
      //   {
      //     errorCachingMinTtl: 300, // in seconds
      //     errorCode: 403,
      //     responseCode: 200,
      //     responsePagePath: '/index.html'
      //   },
      //   {
      //     errorCachingMinTtl: 300, // in seconds
      //     errorCode: 404,
      //     responseCode: 200,
      //     responsePagePath: '/index.html',
      //   },
      // ]
    });

    new ARecord(this, 'apex-record', {
      zone: props.hostedZone,
      recordName: 'cdktraining.afradet.fr.',
      target: {
        aliasTarget: new CloudFrontTarget(cloudFrontDistrib)
      }
    })
  }
}
