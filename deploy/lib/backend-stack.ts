import * as cdk from "aws-cdk-lib";
import {Construct} from "constructs";
import * as lambda from "aws-cdk-lib/aws-lambda"
import * as apigw from "aws-cdk-lib/aws-apigatewayv2";
import * as apigwi from "aws-cdk-lib/aws-apigatewayv2-integrations";

export class BackendStack extends cdk.Stack {

  api: apigw.HttpApi;

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vanillaLambda = new lambda.Function(this, 'vanilla-lambda', {
      runtime: lambda.Runtime.JAVA_21,
      handler: 'fr.afradet.cdktraining.App',
      code: lambda.Code.fromAsset("../back/java-vanilla/target/java-vanilla.jar")
    });

    this.api = new apigw.HttpApi(this, 'apigw-endpoint', {
      apiName: 'cdktraining-api'
    });

    this.api.addRoutes({
      path: '/api/vanilla/greetings',
      integration: new apigwi.HttpLambdaIntegration('vanilla-lambda-integration', vanillaLambda)
    })
  }
}
