import * as cdk from "aws-cdk-lib";
import {Construct} from "constructs";
import * as acm from 'aws-cdk-lib/aws-certificatemanager';
import {CertificateValidation} from 'aws-cdk-lib/aws-certificatemanager';
import * as route53 from "aws-cdk-lib/aws-route53";

export class Certificate extends cdk.Stack {
  certificate: acm.Certificate;
  hostedZone: route53.IHostedZone;

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const domainName = 'cdktraining.afradet.fr';
    const certName = domainName + '-cert';
    this.hostedZone = route53.HostedZone.fromLookup(this, 'hosted-zone', {
      domainName: domainName,
      privateZone: false
    });

    this.certificate = new acm.Certificate(this, certName, {
      domainName: domainName,
      subjectAlternativeNames: [
        '*.' + domainName
      ],
      certificateName: certName,
      validation: CertificateValidation.fromDns(this.hostedZone)
    });
  }
}
