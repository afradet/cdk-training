import * as cdk from 'aws-cdk-lib';
import {StackProps} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as s3Deployment from 'aws-cdk-lib/aws-s3-deployment'
import {OriginAccessIdentity} from 'aws-cdk-lib/aws-cloudfront';
import {PolicyStatement} from "aws-cdk-lib/aws-iam";


export class FrontendStack extends cdk.Stack {
  bucket: s3.Bucket;
  bucketAccessIdentity: OriginAccessIdentity;

  constructor(scope: Construct, id: string, props: StackProps) {
    super(scope, id, props);

    // Removed PublicReadAccess: true as this will cause an error given S3 new default block all.
    this.bucket = new s3.Bucket(this, 'CdkTrainingBucket', {
      bucketName: 'cdktraining.afradet.fr',
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      autoDeleteObjects: true,
      // Specific webapp deploy
      websiteIndexDocument: "index.html"
    });

    // Deploy of frontend dist on bucket
    new s3Deployment.BucketDeployment(this, "deployStaticWebsite", {
      sources: [s3Deployment.Source.asset("../front/dist/app-front/browser")],
      destinationBucket: this.bucket,
    });

    // Access policy for S3, allowing only CloudFront to access content (avoid bucket being public)
    this.bucketAccessIdentity = new OriginAccessIdentity(
        this,
        'bucket-policy-cf',
    );
    const cloudfrontUserAccessPolicy = new PolicyStatement();
    cloudfrontUserAccessPolicy.addActions("s3:GetObject");
    cloudfrontUserAccessPolicy.addPrincipals(this.bucketAccessIdentity.grantPrincipal);
    cloudfrontUserAccessPolicy.addResources(
        this.bucket.arnForObjects("*")
    );
    this.bucket.addToResourcePolicy(cloudfrontUserAccessPolicy);
  }
}
