#!/bin/sh
aws --profile oidc sts assume-role --role-arn arn:aws:iam::427643215870:role/cdk_training_gitlabci_assume_role_deployer  --role-session-name GitLabRunner | ~/jq-linux64 '.Credentials + {"Version": 1}'
