package fr.afradet.cdktraining;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.lambda.runtime.logging.LogLevel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class App implements
    RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

  @Override
  public APIGatewayProxyResponseEvent handleRequest(
      final APIGatewayProxyRequestEvent input,
      final Context context
  ) {
      LambdaLogger logger = context.getLogger();
      JSONObject response = new JSONObject();
    try {
      var body = (JSONObject) new JSONParser().parse(input.getBody());
      var username = body.get("username");
      logger.log("Username received : %s".formatted(username), LogLevel.INFO);
      response.put("content", "Hello %s, you succeed in contacting the lambda".formatted(username));
      return new APIGatewayProxyResponseEvent().withBody(response.toJSONString()).withStatusCode(200);
    } catch (ParseException e) {
      logger.log("Parsing error %s".formatted(e.getMessage()), LogLevel.ERROR);
      response.put("error", e.getMessage());
      return new APIGatewayProxyResponseEvent().withBody(response.toJSONString()).withStatusCode(400);
    }

  }
}
