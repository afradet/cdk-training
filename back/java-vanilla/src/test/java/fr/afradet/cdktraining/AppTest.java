package fr.afradet.cdktraining;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.lambda.runtime.logging.LogLevel;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AppTest {

    @Mock
    private Context context;
    @Mock
    private LambdaLogger logger;

    @Test
    public void shouldReturnGreetings() throws IOException, ParseException {
        // GIVEN
        when(context.getLogger()).thenReturn(logger);
        JSONObject data = (JSONObject) new JSONParser().parse(
            new FileReader(getClass().getClassLoader().getResource("test.json").getFile()));
        var request = new APIGatewayProxyRequestEvent();
        request.setBody(data.toJSONString());

        // WHEN
        App function = new App();
        APIGatewayProxyResponseEvent result = function.handleRequest(request, context);

        // THEN
        assertEquals(200, result.getStatusCode());
        assertEquals(
            "{\"content\":\"Hello Bob, you succeed in contacting the lambda\"}",
            result.getBody()
        );
;        verify(logger).log(anyString(), eq(LogLevel.INFO));
    }

    @Test
    public void shouldReturnErrorIfInvaludInput() {
        // GIVEN
        when(context.getLogger()).thenReturn(logger);
        var request = new APIGatewayProxyRequestEvent();
        request.setBody("not a json");

        // WHEN
        App function = new App();
        APIGatewayProxyResponseEvent result = function.handleRequest(request, context);

        // THEN
        assertEquals(400, result.getStatusCode());
        verify(logger).log(anyString(), eq(LogLevel.ERROR));
    }
}
