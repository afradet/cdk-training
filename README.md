# Training CDK/ops deploying a full app

## Technos
- CDK
- Angular
- Java Vanilla

## Goals

### v1
- [ ] Basic Angular app deployed with s3/cloudfront
- [ ] Java lambda with api gateway
- [x] Basic ci/cd
- [x] Use gitlab as oidc provider for ci authentication (done with console)
- [x] Deleguated Route 53 as dns service for subdomain (done with aws console)
- [ ] Manage certificate with route53

### v2
- [ ] Add cognito authent to angular
- [ ] Lambda authorized
- [ ] Retrieve token from lambda
- [ ] Use rds DB
- [ ] Setup iam authentication

### Next
- [ ] Manage preprod env
- [ ] Run cypress e2e
- [ ] Env by branch (one schema by env)
- [ ] Add pretty/ling
- [ ] Add sonarqube
- [ ] Manage env conf in back and front
- [ ] Calculate and display code coverage
- [ ] Deploy multi lambda in multi languages/fwk
